#clean this up later, everything in one file for now
import sqlite3
from bs4 import BeautifulSoup
from urllib.request import urlopen
from flask import redirect, url_for, request, g, render_template, Flask
app = Flask(__name__)
#IMPORTANT: Create a database with sqlite3 and use the basicdatabase.sql file
DATABASE = 'watchlist.db'

#connect to the local database, possibly move this to its own functions file
def get_db():
    db = getattr(g, '_database', None)
    #if the database isn't connected, connect
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.route('/')
def index():
    #need to clean this up, make a function
    allmovies = get_db().execute("SELECT * FROM movies")
    randommovie = get_db().execute("SELECT * FROM movies ORDER BY RANDOM() LIMIT 1")
    movies = [dict(title=row[1],imdbvar=row[2]) for row in allmovies.fetchall()]
    randomMovie = [dict(title=row[1],imdbvar=row[2]) for row in randommovie.fetchall()]
    allmovies.close()
    randommovie.close()
    return render_template('index.html', movies=movies, randomMovie=randomMovie)

@app.route('/add')
def addMoviePage():
    return render_template('add.html')

@app.route('/movie/<movievar>')
def scrapeMovie(movievar):
    try:
        r = urlopen("http://www.imdb.com/title/%s/" % movievar)
        soup = BeautifulSoup(r, 'html.parser')
        image = soup.find(itemprop="image")
        title = soup.find(itemprop="name")
        rating = soup.find(itemprop="ratingValue")
        image = image['src']
        title = title.get_text().strip()
        if (rating != None):
            rating = rating.get_text().strip()
        else:
            rating = "No rating"
        movieItems = {"image": image, "title": title, "rating": rating, "movievar": movievar}
        return render_template('movie.html', **movieItems)
    except:
        return render_template('404.html')


@app.route('/addmovie/', methods=['GET', 'POST'])
def addMovie():
    try:
        title = request.form['title']
        imdbvar = request.form['imdbvar']
        sep = "?"
        split = imdbvar.split(sep, 1)[0]
        #Get the INT from the imdb url
        imdbvarint = ''.join(x for x in split if x.isdigit())
        cur = get_db()
        cur.execute("INSERT INTO movies ('title', 'imdbvar') VALUES (?, ?)", (title, "tt"+imdbvarint))
        cur.commit()
        cur.close()
        return redirect(url_for('index'))
    except Exception as e:
        return(str(e))
#run the app
if __name__ == "__main__":
    app.run(host="127.0.0.1")

