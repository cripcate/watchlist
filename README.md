# Watchlist

This is a program to create a shared "watchlist". Contributers recieve 
an invitation and share videos, movies, music and booktitles. The list
is viewable at all times on a webpage. 

The Program shall be developed as a webapp using python and flask. The
basic functionality should of course come first. Then further features
or graphic details can be established to make the experience most enjoyable. 

## Vital Parts of the Program:

* Database to store Titles
* Interface to input data into the system
* Frontend to let users view the data

## Later Features

* lookup information for the titles
* weblink support
* analysis of the data
	* pattern recognition 
	* dynamic recommendation
* Im- / Export of lists 
	* dynamic sharing with other groups on a platform

## Development state

1. Learn about flask and webframeworks
2. Rent a webdomain with support for it or set one up on the RPi

